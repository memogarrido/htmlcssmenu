<?php

include '../models/Usuario.php';
session_start();
if (!(isset($_SESSION['usuario']) && !empty($_SESSION['usuario']))) {
    
        header('Location: /inicio.php');
}
$usuario = new Usuario();

$usuariosLst = $usuario->obtenerUsuarios();
echo json_encode($usuariosLst);
/*
echo "<table>";
foreach ($usuariosLst as $usuarios){
    echo "<tr><td>" . $usuarios->nombre . "</td><td>" . $usuarios->edad . "</td></tr>";
}
echo "</table>";
*/