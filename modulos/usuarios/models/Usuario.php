<?php
/**
 * Description of Usuario
 *
 * @author memogarrido
 */
class Usuario {
    public $nombre;
    public $edad;
    
  
        
    function getNombre() {
        return $this->nombre;
    }

    function getEdad() {
        return $this->edad;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }
    
    function obtenerUsuarios(){
        $lstUsuarios = [];
        $sql = "select * from usuarios";
        try {
            $db = new PDO('mysql:host=inncosys.cloudapp.net:62380;dbname=id1217344_ittweb;', 'id1217344_ittweb', '**ittweb**');       
            $stmt = $db->prepare($sql);
            $stmt->execute();
            while ($row = $stmt->fetch()) {

                $usuario = new Usuario();
                $usuario->setNombre($row["nombre"]);
                $usuario->setEdad($row["edad"]);
                array_push($lstUsuarios, $usuario);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $lstUsuarios;
    
    }
    function registrarUsuario($usuario) {
        $sql = "INSERT INTO usuario (`edad`,`nombre`) VALUES (?,?);";
        try {
            $stmt = $this->db->prepare($sql);           
            if ($stmt->execute([$usuario->getEdad(), $usuario->getNombre()])) {
                return 1;
            }
        } catch (Exception $e) {
            echo json_encode($e);
            return -1;
        }
    }


}
