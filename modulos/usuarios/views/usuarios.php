<?php
session_start();
if (!(isset($_SESSION['usuario']) && !empty($_SESSION['usuario']))) {
    
        header('Location: /inicio.php');
}
ob_start();
?>
<h1>Usuarios</h1>
<form action="/modulos/usuarios/controllers/registrarUsuario.php" method="POST">
    <input type="number"  name="edad"/>
    <br/>
    <input type="text" name="nombre"/>
    <input type="submit" value="Enviar"/>
</form>
<button onclick="consultarUsuarios()">Consultar</button>
<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Edad</th>
        </tr>
    </thead>
    <tbody id="tblUsuarios">

    </tbody>
</table>
<?php
$main = ob_get_contents();
ob_clean();
ob_start();
?>
<script type="text/javascript">
    $(document).ready(function () {
    });
    function consultarUsuarios() {
        var configuracion = {};
        configuracion.beforeSend = antesDeEnviar;
        configuracion.complete = peticionTerminada;
        configuracion.data = {max: 10};
        configuracion.dataType = "json";
        configuracion.error = manejarError;
        configuracion.success = peticionTerminadaConExito;
        $.ajax("/modulos/usuarios/controllers/consultarUsuarios.php", configuracion);

    }
    function antesDeEnviar(jqXHR, config) {
        console.log("antes de enviar");
        return 1;
    }
    function peticionTerminadaConExito(arrUsuarios, textStatus, jqXHR) {
        console.log("EXITO" + textStatus + " ");
        console.log(arrUsuarios);
        for (var i = 0; i < arrUsuarios.length; i++) {
            var nombre = arrUsuarios[i].nombre;
            var edad = arrUsuarios[i].edad;
            $("#tblUsuarios").append(
                    "<tr><td>" +
                    nombre +
                    "</td><td>" +
                    edad +
                    "</td></tr>"
                    );
        }
    }
    function peticionTerminada(jqXHR, textStatus) {

    }
    function manejarError(jqXHR, textStatus, errorThrown) {
        console.log("ERROR" + textStatus + " " + errorThrown);
    }
</script>
<?php
$scripts = ob_get_contents();
ob_clean();
include '../../../views/template.php';
