<!DOCTYPE html>
<html  class="htmlFondo">
    <head>
        <meta charset="UTF-8">
        <title>Mi Sitio Web</title>
        <link rel="stylesheet" href="/css/style.css" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    </head>
    <body>
        <form class="divInicioSesion" action="modulos/usuarios/controllers/iniciarSesion.php" method="post">
            <br>
            <label for="mail">Correo:</label>
            <input id="mail" type="email" name="mail"
                   placeholder="Correo" onchange="validarCorreoJQ();"/>
            <br/>
            <br>
            <label for="pass">Password:</label>
            <input id="pass" type="password" name="pass" placeholder="Password"/>
            <br/>
            <button onclick="iniciarSesion();" type="submit" >Iniciar Sesión</button>
            <p id="mensajes"><?php echo $_GET["error"] ?></p>
        </form>

        <script type="text/javascript">
            //alert("hola mundo JS");
        </script>
        <script type="text/javascript" src="/js/scripts.js" ></script>
    </body>
</html>
