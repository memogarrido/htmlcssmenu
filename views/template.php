<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mi Sitio Web</title>
        <link rel="stylesheet" href="/css/style.css" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    </head>
    <body>
        <header>
            <img src="http://toluca.tecnm.mx/wp-content/uploads/2017/02/SLIDER.jpg" alt="LOGO HEADER PAG. ITT"/>
            <nav>
                <ul>
                   
                    <li  class="activo"><a href="/">HOME</a></li>
                    <li><a href="#seccionInfo">Información</a></li>
                    <li><a href="/inicio.php">Iniciar Sesión</a></li>
                    <li><a href="/registro.php">Registro</a></li>
                    <li><a target="_blank" title="Abrirá la pag. principal" href="http://inncosys.com">INNCOSYS</a></li>
                </ul>
            </nav>
        </header>
       <?php 
       if(isset($main) && !empty($main)){
           echo $main;
       }
       ?>
        <footer>Desarrollado por G.G.</footer>
       <?php 
       if(isset($scripts) && !empty($scripts)){
           echo $scripts;
       }
       ?> 
    </body>
</html>
